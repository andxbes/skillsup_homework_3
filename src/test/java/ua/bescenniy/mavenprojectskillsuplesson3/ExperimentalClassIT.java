/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ua.bescenniy.mavenprojectskillsuplesson3;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;



/**
 *
 * @author Andr
 */
public class ExperimentalClassIT {
   private  ExperimentalClass t;
    private  ExperimentalClass testMock;

    
    @Before
    public void init() {
	t = new ExperimentalClass();
	testMock = spy(t);
	System.out.println("\nRandomField = "+ t.getRandomNumber());
    }

    @Test
    public void isUnderReturntDate() {
	fixMethod((t.getRandomNumber() >= 0.5),0.1f);
	System.out.println(t.getRandomNumber());
	assertTrue(t.DataOrTimeWithData(), t.getRandomNumber() < 0.5);

    }

    @Test
    public void isUpwardReturnTimePlusDate() {
	fixMethod((t.getRandomNumber() < 0.5),0.9f);
	System.out.println(t.getRandomNumber());
	assertFalse(t.DataOrTimeWithData(), t.getRandomNumber() < 0.5);
    }
    
    //check ,if "true" change on "f" value
    private void fixMethod(boolean b,float f) {
	if (b) {
	    when(testMock.getRandomNumber()).thenReturn(f);
	    System.out.println(t.DataOrTimeWithData());
	    t = testMock;
	    System.out.println("Fix");
	    return;
	}
	System.out.println("is Ok");
    }
}

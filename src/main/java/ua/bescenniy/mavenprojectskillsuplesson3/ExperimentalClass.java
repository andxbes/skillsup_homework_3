/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ua.bescenniy.mavenprojectskillsuplesson3;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 *
 * @author Andr
 */
public class ExperimentalClass {
    private float f;
    private Random r;

    public ExperimentalClass() {
	r = new Random();
	next();
    }

    public float getRandomNumber() {
	return f;
    }

    private void next() {
	f = r.nextFloat();
    }

    public String DataOrTimeWithData() {
	GregorianCalendar gCalendar = new GregorianCalendar();
	StringBuilder sb = new StringBuilder();
	sb.append("Error!!! Message :\n");
	
	if (f >= 0.5) {
	    sb.append("Time : ").
		    append(gCalendar.get(Calendar.HOUR_OF_DAY)).
		    append(".").append(gCalendar.get(Calendar.MINUTE)).
		    append(".").append(gCalendar.get(Calendar.SECOND)).append(" + ");

	}
	sb.append("Date : ").
		append(gCalendar.get(Calendar.DAY_OF_MONTH)).
		append(".").append(gCalendar.get(Calendar.MONTH) + 1).
		append(".").append(gCalendar.get(Calendar.YEAR));

	return sb.toString();
    }

    
}
//